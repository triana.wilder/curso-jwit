import React from 'react'
import styled from 'styled-components'


const Container = styled.div`
    height: 150px;
    width: 380px;
    border-radius: 25px;
    background-color: #fff0ea;
    
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 10px;
      
`;

const Title = styled.p`
    color: #ff8b60;
    text-transform: uppercase;
    font-size: 15px;
    font-weight: bold;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    width: 300px;
    text-align: center;
`;

const Button = styled.button`
    background-color: #ff8b60;
    text-transform: uppercase;
    border: 2px solid #ff8b60;
    padding: 7px;
    padding-left: 25px;
    padding-right: 25px;
    color: white;
    font-weight: bold;
    border-radius: 25px;
    opacity: 0.6;
    

    cursor: pointer;

`;

const Card = () => {
  return (
    <Container>
        <Title>
              use code: pigi 100 for rs. 100 off on your first order!
        </Title>
        <Button>
            grab now
        </Button>


    </Container>
  )
}

export default Card