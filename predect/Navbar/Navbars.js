import React from 'react'
import styled from 'styled-components';
import menu from '../images/menu.png';
import carrito from '../images/carrito.png';
import lupa from '../images/lupa.png';



const Container =  styled.div`
    
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
   margin-bottom: 1px;
`; 

const Icon = styled.img`
    width: 35px;
    height: 35px;
    border-radius: 50%;
    cursor: pointer;
`;

const Inputs = styled.input`
    width: 250px;
    height: 30px;
    border-radius: 5px ;
    border-color: #E5E6E5;
    background-color: #EDEFEE;
    border-width: 0px;
`;
const Icon2 = styled.img`
  
	position: absolute;
	width: 20px;
	height: 20px;
	left: 90px;
	top: 30px;
	transform: translateY(-50%);
  cursor: pointer;
`;
   

const Navbars = () => {
  return (
   <Container>
        <Icon src={menu}/>
        <Inputs type='text' />
        <Icon2 src={lupa}/>
        <Icon src={carrito}/>
   </Container>
  )
}

export default Navbars