import styled from "styled-components";
import Card from "./Card";
import Submenu from './Submenu';
import Products from './Products';
import Navbar from './Navbar';
import { useState } from "react";


const Container = styled.div`
   
      
`;

function App() {

  const [data, setData] = useState(

    [
      { 
        id: 1,
        color: '#21d0d0', 
        options : [
          { title: 'PRICE LOW TO HIGH', state: false},
          { title: 'PRICE HIGH TO LOW', state: false},
          { title: 'POPULARITY', state: true},
      ],
        footer: 1
      },
      { 
        id: 2,
        color: '#ff7745', 
        options : [
          { title: '1UP NUTRIRION', state: false},
          { title: 'ASITIS', state: false},
          { title: 'AVVATAR', state: true},
          { title: 'BIG MUSCLES', state: false},
          { title: 'BPI SPORTS', state: false},
          { title: 'BSN', state: false},
          { title: 'CELLUCOR', state: false},
          { title: 'DOMIN8R', state: false},
          { title: 'DYMATIZE', state: false},
          { title: 'DYNAMIK', state: false},
          { title: 'ESN', state: false},
          { title: 'EVLUTION NUTRITION', state: false},
      ],
      footer: 2 
        
        },
        
    ]
  )
 

  const handleChange = ( value, id ) => {
       
    const match = data.filter( el => el.id === id ) [0];    
    const act = match.options.filter( el => el.title === value ) [0];
    const copy = [...match.options]
    const targetIndex = copy.findIndex( f => f.title === value);

    if (targetIndex > -1) {
      copy[targetIndex] = { title: value, state: !act.state};
      match.options = copy;  
    };

    const copyList = [...data]

    const targetIndexList = copyList.findIndex( f => f.title === id); 

    if (targetIndexList > -1) {
      copy[targetIndexList] = match;     
    };

    setData(copyList)
  
  }

  const handleReset = (id) => {
      
  
    const copy = [...data]

    const targetIndex = copy.findIndex( f => f.id === id); 

    if (targetIndex > -1) {

      var raw = []
      
      const options = data.filter( el => el.id === id) [0].options;

      for ( let x of options) raw.push({ title: x.title, state: false});

      copy[targetIndex].options = raw;     
    };

    setData(copy);
  
    }

  return (

    <Container>
          <Navbar />
           <Card />
           <Products />

           {
            data.map( ( v, i) =>  <Submenu {...v} onChange={ handleChange } onReset={ handleReset }/> )
           }
           
    </Container>
   
  );
}

export default App;
