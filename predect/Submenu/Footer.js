/* eslint-disable no-fallthrough */
import styled from "styled-components";
import { Circulo, Icon} from "./Option";
import check from '../images/check.png';
import cancel from '../images/cancel.png';
import reset from '../images/reset.png';


const Container =  styled.div`
    display: flex;
    justify-content: center; 
    align-items: center;
    padding-bottom: 2px; 
    
`;

const ContainerV1 =  styled.div`
    display: flex;
    justify-content: center; 
    align-items: center;
    margin-top: 15px;
    padding-top: 15px;
    padding-bottom: 15px;
    border-top: 1px solid #ffffff80;
`;

const Item = ({ v, onClick }) => {
    return( <Circulo visible={true} style={{ width:60, height: 60 }} onClick={onClick}>
        <Icon src={v} visible={true} style={{ width:60, height: 60 }}/>
    </Circulo>
)}


const index = ({version, onReset, onCancel, onAcept}) => {

    const handleCancel = () => {
        console.log('vamos a cancelar')
        if ( typeof onCancel === 'function' ) onCancel()
    };

    const handleReset = () => {
                console.log('vamos a resetear')
                if ( typeof onReset === 'function' ) onReset()
    };

        const handleOk = () => {
            console.log('OK')
            if ( typeof onAcept === 'function' ) onAcept()
    }

        
        

   if (version === 1) {

        const data = [
            {icon: cancel, onClick: handleCancel}, 
            {icon: check, onClick: handleOk},
        ]

        return(

        <Container >
        
              {
                  data.map((v, i) => <Item key={i} v={v.icon} onClick={v.onClick} />)}
           
      </Container>
        )

   } if (version === 2) {

    const data = [
        {icon: cancel, onClick: handleCancel}, 
        {icon: reset, onClick: handleReset},
        {icon: check, onClick: handleOk},
    ]

    return (
        <ContainerV1 >
          
        {
            data.map((v, i) => <Item key={i} v={v.icon} onClick={v.onClick} />)
        }
     
</ContainerV1>
    )
  

   }
  
    switch ( version )  {
        case 1 : { 
         
        }

        case 2 : {
            

        }

        default : return <></>
    };
  
    
}

export default index;