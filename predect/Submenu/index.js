import styled from "styled-components";
import Option from "./Option";
import Footer from './Footer';


const Container =  styled.div`
    
    margin-top: 15px;
    background-color: ${props => props.color};
    border-radius: 25px;
    margin-bottom: 15px;
    //height: ${props => props.height};
    width: 400px;
    padding-top: 15px;
    //opacity: 0.8;
    
`;


const index = (params) => {
  
    /*
    color: string
    options; string[]
    
    */
   const handleChange = (value) => {
    if ( typeof params.onChange === 'function' ) params.onChange(value, params.id);
    }
    
    const handleReset = () => {
        if ( typeof params.onReset === 'function' ) params.onReset( params.id);
        }

    return (
        <Container color={params.color} >
              
              { params.options.map( (v, i) => <Option  key={i} {...v} onChange={ handleChange } /> )}
            
            <Footer  version={params.footer} onReset={ handleReset }/>
                
                

        </Container>
  )
}

export default index;