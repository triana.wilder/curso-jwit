/* eslint-disable react-hooks/rules-of-hooks */

import styled from 'styled-components';
import check from '../images/check.png'

const Container =  styled.div`
    margin-top: 15px;
    margin-left: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    margin: 0px;
    color: white;
    font-family:  -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: 500;
`;

 export const Circulo = styled.div`
    border-radius: 50%;
    border: 1.8px solid white;
    width: 35px;
    height: 35px;
    cursor: pointer;
    background-color: ${ props => props.visible ? 'white' : 'transparent'};
    margin-left: 5px;
`;

export const Icon = styled.img`
    width: 35px;
    height: 35px;
    border-radius: 50%;
    visibility: ${ props => props.visible ? 'visible' : 'hidden'};
   
`;

const option = ( { title, state, onChange} ) => {
    
  
    
    const handleChange = () => {
          
          if ( typeof onChange === 'function' ) onChange(title);
          
    }

  return (
    <Container>
            <Title>{title}</Title>
            <Circulo visible={state} onClick={ handleChange }>
                <Icon src={ check } visible={state}/>
            </Circulo>
           
            
    </Container>
  )
}

export default option;
