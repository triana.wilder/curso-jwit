

import styled from 'styled-components';
import protein1 from '../images/protein1.png';
import protein2 from '../images/protein2.png';


const Container =  styled.div`
    
    
    display: flex;
    flex-direction: row;
    justify-content: space-between;
   margin-bottom: 1px;
`;

const Title = styled.p`
    color: gray;
    font-size: 13px;
    font-weight: bold;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    width: 100%;
    text-align: center;
    display: table;
    cursor: pointer;
    
    
`;

const Imagen = styled.img`
    width: 100%;
    margin-bottom: 1px;
   
`;
   
const options = [
  { title: 'PROTEINS', state: true},
  { title: 'PRE/POST WORKOUT', state: false},
  { title: 'OTHERS', state: false},

] 

const OptionsProducts = ( ) => {
    
  



  return (
    <>
    <Container>
             {
                options.map( (v, i) =>  <Title key={i}>{v.title}</Title> )

              }
    </Container>
    <Container>
              <Imagen src={protein1}/>
              <Imagen src={protein2}/>
    </Container>
    <Container>
              <Title>WHEY PROTEINS</Title>
              <Title>WHEY INSOLATES</Title>
              
    </Container>
    </>
  )
}

export default OptionsProducts;
