import styled from "styled-components";
import Checkbox from './checkbox';
import check from '../images/check.png';
import cancel from '../images/cancel.png';
import reset from '../images/reset.png';
import { useState } from "react";


const Container =  styled.div`
    display: flex;
    justify-content: center; 
    align-items: center;
    padding-bottom: 2px; 
`;

const ContainerV1 =  styled.div`
    display: flex;
    justify-content: center; 
    align-items: center;
    margin-top: 15px;
    padding-top: 15px;
    padding-bottom: 15px;
    border-top: 1px solid #ffffff80;
`;

export interface option {
    icon: string;
    onClick: () => void; 
}

export interface params {
    enableReset: boolean;
    enableCancel: boolean;
}


const App = (): JSX.Element => {
 
    const [options, setOptions] = useState<option[]>([
        {icon: check, onClick: () => {}}
    ])

    const handleCancel = () => {
        console.log('vamos a cancelar')
        if ( typeof onCancel === 'function' ) onCancel()
    };

    const handleReset = () => {
                console.log('vamos a resetear')
                if ( typeof onReset === 'function' ) onReset()
    };

        const handleOk = () => {
            console.log('OK')
            if ( typeof onAcept === 'function' ) onAcept()
    };

    const data: option[] = [
        {icon: cancel, onClick: handleCancel}, 
        {icon: reset, onClick: handleReset},
        {icon: check, onClick: handleOk},
    ]




    return (
        <Container >
        
        {
            data.map((v, i) => <Checkbox image={check} onClick={ v.onClick } />)}
     
</Container>
    )
  }
  
  export default App
  