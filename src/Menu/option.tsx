import styled from 'styled-components';
import Checkbox from './checkbox';
import check from '../images/check.png';


const Container =  styled.div`
    margin-top: 15px;
    margin-left: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`;

const Title = styled.p`
    margin: 0px;
    color: white;
    font-family:  -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: 500;
`;

 

export interface params  {
    value: string;
    onClick?: ( params : {value: string, state: boolean}) => void;
   
}

const App = (params: params): JSX.Element => {
 
    const handleClick = ( state: boolean ) =>  {
        if( typeof params.onClick === 'function') {
            params.onClick( {value: params.value, state})}
    };
   

    return (
        <Container>
            <Title>
                {params.value}
            </Title>
            <Checkbox image={check} onClick={handleClick} />
       
        
</Container>
    )
  }
  
  export default App
  